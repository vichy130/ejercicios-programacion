using System;

namespace ejercicio_03
{
    class Program
    {
                /* Programa en C#, para compilar en linea : https://rextester.com/JUCDZ52986
         Para cambiar el valor en consola, dar click en show input e ingresar otro valor*/

        static void Main(string[] args)
        {
            double dolares;
            double pesos = 0;
            double CURRENCY= 22.44;
            string mensaje_error = "Cantidad no valida";

            Console.WriteLine("Ingresa un monto en d�lares ( USD ): ");
            dolares = Double.Parse(Console.ReadLine());

            if (dolares > 0)
            {
                pesos = (dolares * CURRENCY);
                Console.WriteLine("La cantidad en pesos es: "+pesos);
            }
            else
            {
                Console.WriteLine(mensaje_error);
            }
        }
    }
}
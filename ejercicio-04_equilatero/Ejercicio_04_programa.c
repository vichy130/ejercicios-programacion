using System;

namespace ejercicio_04
{
    class Program
    {
        /* Programa en C#, para compilar en linea : https://rextester.com/BFC2844
        Para cambiar el valor en consola, dar click en show input e ingresar otro valor*/

        static void Main(string[] args)
        {
            Console.WriteLine("Calculo de per�metro de un tri�ngulo Equilatero");

            double medida = 0;
            double perimetro = 0;
            int LADOS = 3;

            Console.WriteLine("\n �Cual sera la medida de los lados del triangulo? (CM): ");
            medida = double.Parse(Console.ReadLine());

            perimetro = LADOS * medida;
            Console.WriteLine("El perimetro del triangulo es: "+perimetro);
        }
    }
}

Inicio
    variable double ladoA=0
    variable double ladoB=0
    variable double ladoC=0
    variable double perimetro=0
    Leer "Ingresa la medida del lado 'a': ", ladoA
    Leer "Ingresa la medida del lado 'b': ", ladoB
    Leer "Ingresa la medida del lado 'c': ", ladoC

    si (ladoA > 0 && ladoB > 0 && ladoC>0) entonces
	perimetro = ladoA + ladoB+ ladoC
	Escribir "El perimetro del triangulo Escaleno es: ",perimetro
    Si no
	Escribir "los valores deben ser mayores a 0"
    Fin-Si
Fin
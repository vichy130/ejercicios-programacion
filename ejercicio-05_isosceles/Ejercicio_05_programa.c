using System;

namespace ejercicio_05
{
    class Program
    {
        static void Main(string[] args)
        {
         /* Programa en C#, para compilar en linea :https://rextester.com/VSEPI77451
         Para cambiar el valor en consola, dar click en show input e ingresar cada 
         valor en una linea*/

            Console.WriteLine("Triángulo Isósceles");

            double ladoA = 0;
            double ladoB = 0;
            double perimetro = 0;

            Console.WriteLine("Ingresa la medida del lado 'a' (lado que se repite): ");
            ladoA = Double.Parse(Console.ReadLine());
            dolares = Double.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa la medida del lado 'b': ");
            ladoB = Double.Parse(Console.ReadLine());

            if(ladoA>0 && ladoB>0)
            {
                perimetro = (2 * ladoA) + ladoB;
                Console.WriteLine("El perimetro del triangulo Isósceles es: "+perimetro);
			}else
            {
                Console.WriteLine("los valores deben ser mayores a 0");    
			}

        }
    }
}
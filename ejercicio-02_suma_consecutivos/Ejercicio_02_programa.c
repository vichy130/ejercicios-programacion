using System;

namespace ejercicio_02
{
    class Program
    {
        
        /* Programa en C#, para compilar en linea : https://rextester.com/SVLAV13644
         Para cambiar el valor en consola, dar click en show input e ingresar otro valor*/

        static void Main(string[] args)
        {
            int NUMERO_MINIMO = 1, NUMERO_MAXIMO = 50;
            int  suma = 0, numero = 0, temporal =1;
            string mensaje_error = "El numero ingresado no es valido";

            Console.WriteLine("ingresa un numero entre 1-50:");
            numero=Convert.ToInt32(Console.ReadLine());

                if (validaNumero(numero, NUMERO_MINIMO, NUMERO_MAXIMO))
                {
                    while (temporal <= numero)
                    {
                        suma = suma +temporal;
                        temporal++;
                    }
                Console.WriteLine("La suma de los n�meros consecutivos del "+NUMERO_MINIMO+" hasta " + numero + " es: " + suma);
                }
                else
                {
                    Console.WriteLine(mensaje_error);
                }

        }

        public static bool validaNumero(int numero, int minimo, int maximo)
        {
            if (numero >= minimo && numero <= maximo)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}